Ducky
=====

A USB/Atmega32U4 "generic" device.

This has a single pushbutton to control the device.  There is also a
reed switch that can be closed by holding a magnet near the device.
A blue LED is also controllable.

The reed switch could be used to control the device operation.  For
instamce, if the reed switch is closed at power up the device might
operate normally.  But if the switch isn't closed at power up the
operation could look more benign.  This is all controlled by the
sketch downloaded.

Has been used to implement:

+----------------+----------------------------------------------+
| PWKeep         | a password keeper thing that types passwords |
+----------------+----------------------------------------------+
| mouse_jiggler  | device to "jiggle" the mouse cursor          |
+----------------+----------------------------------------------+

The directory *hardware* contains the KiCad data used to build the 
device PCB.

The directory *firmware* contains the different firmwares used to 
make the Ducky device do something useful.

The directory *python* contains python programs to configure, etc,
the various firmware incarnations.
