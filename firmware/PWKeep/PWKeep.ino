//================================================================
// PWKeep
//
// A small program to store passwords and send them to the host as keyboard input.
//
// This version recognizes these events:
//     * a short push (< 200msec),
//     * a long push (> 200msec), and
//     * a timeout (500msec of no action).
//
// The program remembers 10 passwords in memory, each with a single alphabetic name.
// The **morse** character recognized is the name of the saved password.
// A timeout resets the recognizer.
//
// This version is designed to be configured by the python program "pwkeep"
// Set as a "Ducky" board in the Arduino IDE.
// Set programmer as USBTinyISP.
// Program using "Sketch|Upload using Programmer".
//================================================================

#include <EEPROM.h>
#include <Keyboard.h>
#include <stdarg.h>
#include <avr/wdt.h>                    // for the soft-boot code

#define PRODUCT_NAME  "PWKeep"
#define VERSION       "3.1"

// pins for buttons and LEDs
#define ButtonPin     PD7
#define ReedPin       PD4
#define LEDPin        PD6

#define DebounceCount 20

// if NUKE is defined the 'nuke' code is compiled in,
// inserting the device with the button pushed will delete all passwords
// and create some deniable entries
//#define NUKE

// if defined turn on debug
#define DEBUG

#ifdef DEBUG
  #define dprint(...) debug_printf(__VA_ARGS__)
#else
  #define dprint(...)
#endif

// port manipulation to read a port/pin
#define PIN_READ(port, pin) ((PIN ## port & (1 << pin)) != 0)

#define INIT_EEPROM           0         // if > 0 reinitialize EEPROM

//definitions of times for long click and timeout (milliseconds)
#define LongClickTime         200
#define TimeoutPeriod         500

// Finite State Machine events
#define EVENT_TIMEOUT         0         // no activity for a period
#define EVENT_SHORTPUSH       1         // a short push
#define EVENT_LONGPUSH        2         // a longer push

// number of events in the morse buffer (allow 10 dot/dash)
#define MORSE_BUFF_SIZE       10

// macro to turn off warnings about "unused parameter" in functions
#define UNUSED(v) (void) (v)

// OK and error message
const char *OKMsg = "OK";
const char *ErrorMsg = "ERROR";

// the password storage stuff
#define MaxPasswordLen    60      // max length of passwords
#define NumPasswords      16      // number of saved passwords

typedef struct { char password[MaxPasswordLen+1];
                 char ch;
               } password;

// variables restored from and saved to EEPROM
unsigned long Checksum = 0;       // place to save EEPROM data CRC
password Passwords[NumPasswords]; // all the passwords
bool DeviceLocked = false;        // "true" if device locked
char LockPassword = '\0';         // the lock password char

#define EepromPasswordSize  (sizeof(Passwords))

// define start addresses in EEPROM of various fields
#define EepromChecksum      (0)
#define EepromPasswords     (EepromChecksum + sizeof(Checksum))
#define EepromDeviceLocked  (EepromPasswords + sizeof(Passwords))
#define EepromLockPassword  (EepromDeviceLocked + sizeof(DeviceLocked))

// buffer, etc, to gather external command strings
#define MAX_COMMAND_LEN     70
#define COMMAND_END_CHAR    ';'
char CommandBuffer[MAX_COMMAND_LEN + 1];
int CommandIndex = 0;

// buffer to build dynamic messages in
char TmpBuffer[70];

// flag, "true" if NUKE was asked for
bool was_nuked = false;

// define the Morse Node struct
typedef struct MorseNode { struct MorseNode *dot;
                           struct MorseNode *dash;
                           char ch;
                         } MorseNode;

// and define the morse "decode" tree just for alphabetics
const MorseNode PROGMEM m_h = {NULL, NULL, 'H'};
const MorseNode PROGMEM m_v = {NULL, NULL, 'V'};
const MorseNode PROGMEM m_s = {&m_h, &m_v, 'S'};
const MorseNode PROGMEM m_f = {NULL, NULL, 'F'};
const MorseNode PROGMEM m_u = {&m_f, NULL, 'U'};
const MorseNode PROGMEM m_i = {&m_s, &m_u, 'I'};

const MorseNode PROGMEM m_l = {NULL, NULL, 'L'};
const MorseNode PROGMEM m_r = {&m_l, NULL, 'R'};
const MorseNode PROGMEM m_p = {NULL, NULL, 'P'};
const MorseNode PROGMEM m_j = {NULL, NULL, 'J'};
const MorseNode PROGMEM m_w = {&m_p, &m_j, 'W'};
const MorseNode PROGMEM m_a = {&m_r, &m_w, 'A'};

const MorseNode PROGMEM m_e = {&m_i, &m_a, 'E'};

const MorseNode PROGMEM m_b = {NULL, NULL, 'B'};
const MorseNode PROGMEM m_x = {NULL, NULL, 'X'};
const MorseNode PROGMEM m_d = {&m_b, &m_x, 'D'};
const MorseNode PROGMEM m_c = {NULL, NULL, 'C'};
const MorseNode PROGMEM m_y = {NULL, NULL, 'Y'};
const MorseNode PROGMEM m_k = {&m_c, &m_y, 'K'};
const MorseNode PROGMEM m_n = {&m_d, &m_k, 'N'};

const MorseNode PROGMEM m_z = {NULL, NULL, 'Z'};
const MorseNode PROGMEM m_q = {NULL, NULL, 'Q'};
const MorseNode PROGMEM m_g = {&m_z, &m_q, 'G'};
const MorseNode PROGMEM m_o = {NULL, NULL, 'O'};
const MorseNode PROGMEM m_m = {&m_g, &m_o, 'M'};

const MorseNode PROGMEM m_t = {&m_n, &m_m, 'T'};

const MorseNode PROGMEM morse_head = {&m_e, &m_t, (char) 0};

//##############################################################################
// Utility routines.
//##############################################################################

//----------------------------------------
// Little debug printer, controlled by the DEBUG #define
//----------------------------------------

#ifdef DEBUG
void debug_printf(char *fmt, ...)
{
  va_list ap;
  char buff[128];

  va_start(ap, fmt);
  vsprintf(buff, fmt, ap);
  Serial.println(buff);
  va_end(ap);
}
#endif

//----------------------------------------
// Calculate the CRC of the saved passwords in EEPROM
//----------------------------------------

unsigned long eeprom_crc(void)
{

  const unsigned long crc_table[16] = {0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
                                       0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
                                       0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
                                       0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
                                      };
  unsigned long crc = ~0L;

  for (unsigned long index = EepromPasswords ; index < EepromPasswordSize; ++index)
  {
    crc = crc_table[(crc ^ EEPROM[index]) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM[index] >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }

  return crc;
}

//----------------------------------------
// Saves the in-memory password data to EEPROM and refreshes the checksum.
//----------------------------------------

void save_eeprom_data(void)
{

  dprint("  save_eeprom_data: DeviceLocked=%d, LockPassword=%s",
         DeviceLocked, LockPassword);
//#ifdef DEBUG
//  Serial.print("  save_eeprom_data: DeviceLocked=");
//  Serial.print(DeviceLocked);
//  Serial.print(", LockPassword=");
//  Serial.println(LockPassword);
//#endif

  // save in-memory passwords to EEPROM
  EEPROM.put(EepromPasswords, Passwords);
  EEPROM.put(EepromDeviceLocked, DeviceLocked);
  EEPROM.put(EepromLockPassword, LockPassword);

  // calculate CRC of the data
  Checksum = eeprom_crc();
  EEPROM.put(EepromChecksum, Checksum);
}

//----------------------------------------
// Gets the password data in EEPROM and refreshes the RAM copies.
//
// If the EEPROM checksum is invalid, initializes EEPROM and sets
// in-memory copy to initialized values.
//----------------------------------------

void restore_eeprom_data(void)
{
  // get data checksun
  EEPROM.get(EepromChecksum, Checksum);

  // get actual CRC of data and ensure same, if not, initialize
  if (Checksum != eeprom_crc() || INIT_EEPROM != 0)
  {
    // initialize passwords to EMPTY
    for (int pass_num = 0; pass_num < NumPasswords; ++pass_num)
    {
      password *pwd = &Passwords[pass_num];

      pwd->ch = (char) 0;
      for (int i=0; i < MaxPasswordLen; ++i)
        pwd->password[i] = (char) 0;
    }

    // set lock stuff to UNLOCKED
    DeviceLocked = false;
    LockPassword = '\0';

    // if "nuke" flag set, create fake data
    if (was_nuked)
    {
      password *pw = &Passwords[0];
      pw->ch = 'E';
      strcpy(pw->password, "DEADBEEF");

      pw = &Passwords[1];
      pw->ch = 'S';
      strcpy(pw->password, "correct horse battery staple");

      was_nuked = false;
    }

    // save initialized data
    save_eeprom_data();
  }

  // get data stored in EEPROM
  EEPROM.get(EepromChecksum, Checksum);
  EEPROM.get(EepromPasswords, Passwords);
  EEPROM.get(EepromDeviceLocked, DeviceLocked);
  EEPROM.get(EepromLockPassword, LockPassword);

  dprint("restore_eeprom_data: DeviceLocked=%d, LockPassword=%s",
         DeviceLocked, LockPassword);
//#ifdef DEBUG
//  Serial.print("restore_eeprom_data: DeviceLocked=");
//  Serial.print(DeviceLocked);
//  Serial.print(", LockPassword=");
//  Serial.println(LockPassword);
//#endif
}

//----------------------------------------
// Code to reboot an AtMega32U4.
//
// From:
// https://blog.fsck.com/2014/08/how-to-reboot-an-arduino-leonardo-micro-into-the-bootloader.html
//----------------------------------------

void reboot(void)
{
  uint16_t bootKey = 0x7777;
  uint16_t *const bootKeyPtr = (uint16_t *) 0x0800;

  // Stash the magic key
  *bootKeyPtr = bootKey;

  // Set a watchdog timer
  wdt_enable(WDTO_120MS);

  while (1) {} // This infinite loop ensures nothing else
  // happens before the watchdog reboots us
}

//----------------------------------------
// Convert a string to uppercase, in situ.
//----------------------------------------

void str2upper(char *str)
{
  while (*str)
  {
    *str = toupper(*str);
    ++str;
  }
}

//##############################################################################
// The recognizer routines and password processor.
//##############################################################################

#ifdef UNUSED
//----------------------------------------
// Decode an event number to a string.
//----------------------------------------

char * decode_event(int event)
{
  switch (event)
  {
    case EVENT_TIMEOUT:   return (char *) "TIMEOUT";
    case EVENT_SHORTPUSH: return (char *) "SHORTPUSH";
    case EVENT_LONGPUSH:  return (char *) "LONGPUSH";
  }

  sprintf(TmpBuffer, "Unknown event number %d", event);
  return TmpBuffer;
}
#endif

//----------------------------------------
// Accept an event and perform the appropriate action.
//----------------------------------------

void send_password(int morse[], int morse_count)
{
  MorseNode *m_walk = &morse_head;

  // walk the morse decode tree until "bad code" or end of morse data
  for (int i=0; i < morse_count; ++i)
  {
    if (morse[i] == EVENT_SHORTPUSH)
    {
      if (!m_walk->dot)
        return;     // error, unrecognized morse code

      m_walk = m_walk->dot;
    }
    else
    {
      if (!m_walk->dash)
        return;     // error, unrecognized morse code

      m_walk = m_walk->dash;
    }
  }

  // find password slot for that character
  password *found = find_password(m_walk->ch, NULL);

  if (found)
    Keyboard.print(found->password);
}


//----------------------------------------
// Accept an event and perform the appropriate action.
//----------------------------------------

void fsm(int event)
{
  static int morse[MORSE_BUFF_SIZE]; // the "morse" events
  static int morse_count = 0;        // index into "morse[]" of next event slot
  static bool fsm_abort = false;     // "true" if ignoring everything

  // if aborted or buffer overrun, do nothing
  if (fsm_abort || morse_count >= MORSE_BUFF_SIZE)
  {
    fsm_abort = true;
    morse_count = 0;
    return;
  }

  // decide what to do, either save morse char or send password
  switch (event)
  {
    case EVENT_SHORTPUSH:
    case EVENT_LONGPUSH:
      morse[morse_count++] = event;
      break;
    case EVENT_TIMEOUT:
      if (!fsm_abort && morse_count > 0)
        send_password(morse, morse_count);
      morse_count = 0;
      fsm_abort = false;
      break;
    default:
      Serial.print("Bad event=");
      Serial.println(event);
      break;
  }
}

//----------------------------------------
// Handle a button state change.
//
// Decide if we have a SINGLE CLICK, LONG CLICK or TIMEOUT event.
// Call the appropriate handler once we have an event.
//----------------------------------------

void change_button_state(int state)
{
  // state variables to determine if single/long click
  static unsigned long start_click = 0L;    // time of start of click
  unsigned long now = millis();

  if (state)
  {
    // button UP
    if ((now - start_click) <= LongClickTime)
    {
      // a short click
      fsm(EVENT_SHORTPUSH);
    }
    else
    {
      // a long click
      fsm(EVENT_LONGPUSH);
    }

    start_click = 0L;
  }
  else
  {
    // button DOWN
    start_click = now;
  }
}

//----------------------------------------
// Function to handle polling the button - includes debounce.
//
// Passes new (debounced) state to change_button_state() function.
//
// Code here from: https://playground.arduino.cc/Learning/SoftwareDebounce/
//----------------------------------------

unsigned long next_timeout = 0;  // time TIMEOUT event will be raised

void poll_button(void)
{
  // static variables to handle software debounce
  static int current_state = HIGH;        // the debounced input value
  static int counter = 0;                 // how many times we have seen new value
  static unsigned long poll_time = 0;     // the last time the output pin was sampled

  unsigned long now = millis();           // the current time

  // if enough time has passed, poll the button
  if (now > poll_time)
  {
    poll_time = now + 1;                  // we poll every millisecond

    // if button now UNPRESSED and timeout period elapsed, raise TIMEOUT
    if ((current_state == HIGH) && (next_timeout < now))
    {
      fsm(EVENT_TIMEOUT);
      next_timeout = now + TimeoutPeriod;
    }

    int reading = digitalRead(ButtonPin); // current pin state

    if (reading == current_state && counter > 0)
    {
      counter--;
    }

    if (reading != current_state)
    {
       counter++;
    }

    // If the pin has shown the same value for long enough let's switch it
    if (counter >= DebounceCount)
    {
      counter = 0;
      current_state = reading;
      change_button_state(reading);
      next_timeout = now + TimeoutPeriod;   // reset timeout counter
    }
  }
}

//----------------------------------------
// Find a password for a given character.
//     pass_ch     the character to find
//     empty_slot  place to return "first empty" pointer
//                 (no return if NULL)
// Returns the address of the slot matching "pass_ch", else NULL.
//----------------------------------------

password *find_password(char pass_ch, password **empty_slot)
{
  // flag to show if empty slot is the first found
  bool first_empty = true;

  // set return "first empty slot" to NULL (if "empty_slot" specified)
  if (empty_slot)
  {
    *empty_slot = NULL;
  }

  // look for matching slot
  for (int i=0; i < NumPasswords; ++i)
  {
    password *p = &Passwords[i];

    if (p->ch == (char) 0)
    {
      // empty slot, save in empty_slot if first
      if (empty_slot && first_empty)
      {
        *empty_slot = p;
        first_empty = false;
      }
    }
    else if (p->ch == pass_ch)
    {
      // found it
      return p;
    }
  }

  return NULL;
}

//##############################################################################
// Code for the "external" commands from the serial port.
//##############################################################################

//----------------------------------------
// Print debug info:
//     XYZZY
//----------------------------------------

#ifdef DEBUG
void xcmd_debug(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "X"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // print debug stuff
  dprint("Memory: DeviceLocked=%d, LockPassword=%s",
         DeviceLocked, LockPassword);
//  Serial.print("Memory: DeviceLocked=");
//  Serial.print(DeviceLocked);
//  Serial.print(", LockPassword=");
//  Serial.println(LockPassword);

  bool tmp_devicelocked;
  char tmp_lockpassword;

  EEPROM.get(EepromDeviceLocked, tmp_devicelocked);
  EEPROM.get(EepromLockPassword, tmp_lockpassword);

  Serial.print("EEPROM: DeviceLocked=");
  Serial.print(tmp_devicelocked);
  Serial.print(", LockPassword=");
  Serial.println(tmp_lockpassword);
}
#endif

//----------------------------------------
// Get help:
//     H
//----------------------------------------

void xcmd_help(char *cmd)
{
  UNUSED(cmd);

#ifdef NUKE
  Serial.println(F("~~~~~~~~~~ Configuration Commands ~~~~~~~~~~~~~~"));
#else
  Serial.println(F("---------- Configuration Commands --------------"));
#endif

  Serial.println(F("H;       display help text"));
  Serial.println(F("ID;      show device identifier string"));
  Serial.println(F("CLEAN;   destroy all saved passwords and unlock"));
//  Serial.println(F("LOCKa;   set password for 'a' as lock password"));
//  Serial.println(F("UNLOCK;  unlock device until next boot"));
  Serial.println(F("Da;      delete password for 'a'"));
  Serial.println(F("P;       display all saved passwords"));
  Serial.println(F("Paxxx;   set password for 'a' to 'xxx'"));
  Serial.println(F("R;       reboot the device"));

#ifdef NUKE
  Serial.println(F("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"));
#else
  Serial.println(F("------------------------------------------------"));
#endif
}

//----------------------------------------
// Set the lock password character:
//     LOCKa
//----------------------------------------

void xcmd_lock(char *cmd)
{
  // if not legal, complain
  if (strncmp(cmd, "LOCK", 4) || strlen(cmd) != 5)
  {
    Serial.println(ErrorMsg);
    return;
  }

  // get lock character
  char pass_ch = cmd[4];

  // if there is no password for that character, error
  password *found = find_password(pass_ch, NULL);

  if (!found)
  {
    Serial.println(ErrorMsg);
    return;
  }

  // set "pass_ch" as the password character and lock device
  LockPassword = pass_ch;
  DeviceLocked = true;

  // save updated EEPROM data
  save_eeprom_data();
  DeviceLocked = false;   // unlock until next boot

  Serial.println(OKMsg);
}

//----------------------------------------
// Unlock the device:
//     UNLOCK
//----------------------------------------

void xcmd_unlock(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "UNLOCK"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // unlock the device
  DeviceLocked = false;
  LockPassword = '\0';

  // save updated EEPROM data
  save_eeprom_data();

  Serial.println(OKMsg);
}

//----------------------------------------
// Get the identifier string:
//     ID
//----------------------------------------

void xcmd_id(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "ID"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // generate ID string and return
  Serial.print(PRODUCT_NAME);
  Serial.print(" ");
  Serial.println(VERSION);
}

//----------------------------------------
// Destroy any saved passwords and other information:
//     CLEAN
//----------------------------------------

void xcmd_clean(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "CLEAN"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // ensure in-memory buffers clean and ALL EEPROM bytes wiped
  for (int pass_num = 0; pass_num < NumPasswords; ++pass_num)
  {
    Passwords[pass_num].ch = (char) 0;

    for (int i = 0; i < MaxPasswordLen+1; ++i)
    {
      Passwords[pass_num].password[i] = ' ';
    }
  }

  // save cleaned EEPROM data
  save_eeprom_data();

  Serial.println(OKMsg);
}

//----------------------------------------
// Destroy any saved password information:
//     Da
//----------------------------------------

void xcmd_del(char *cmd)
{
  // if wrong length, ERROR!
  if (strlen(cmd) != 2)
  {
    Serial.println(ErrorMsg);
    return;
  }

  // get password char
  char pass_ch = cmd[1];

  // find password slot or first empty slot
  password *found = find_password(pass_ch, NULL);

  if (!found)
  {
    Serial.println(ErrorMsg);
    return;
  }

  // if this is the LOCKED password unlock device
  if (found->ch == LockPassword)
  {
    DeviceLocked = false;
    LockPassword = '\0';
  }

  // destroy data in password slot
  found->ch = (char) 0;
  for (int i=0; i < MaxPasswordLen+1; ++i)
  {
    found->password[i] = ' ';
  }

  save_eeprom_data();

  Serial.println(OKMsg);
}

//----------------------------------------
// Set/Get password string:
//     P       display all saved passwords
//     Paxxxx  set password 'a'
//
// Because we might have a password here, the 'cmd' string has
// not been converted to uppercase.
//----------------------------------------

void xcmd_password(char *cmd)
{
  // if just "P" print all saved passwords
  if (strlen(cmd) == 1)
  {
    bool was_output = false;  // set to "true" if any password is printed

    // display all saved passwords
    for (int pass_num = 0; pass_num < NumPasswords; ++pass_num)
    {
      if (Passwords[pass_num].ch != 0)
      {
        Serial.print(Passwords[pass_num].ch);
        Serial.print(Passwords[pass_num].ch == LockPassword ? "*" : " ");
        Serial.print(" '");
        Serial.print(Passwords[pass_num].password);
        Serial.println("'");

        was_output = true;
      }
    }

    if (!was_output)
    {
      // if no passwords printed, OK response
      Serial.println(OKMsg);
    }

    return;
  }

  // get UPPERCASE password char and check valid, ie, A-Z
  char pass_ch = toupper(cmd[1]);

  if (pass_ch < 'A'|| pass_ch > 'Z')
  {
    Serial.println(ErrorMsg);
    return;
  }

  // setting a password, get the new password, check length
  char *new_password = &cmd[2];

  if (strlen(new_password) < 1)
  {
    Serial.println(ErrorMsg);
    return;
  }

  // find password slot or first empty slot
  password *next = NULL;
  password *found = find_password(pass_ch, &next);

  // see if we found a slot to use
  if (!found)
    found = next;

  if (found)
  {
    // there is an empty slot, save new password
    if (strlen(new_password) >= MaxPasswordLen)
    {
      Serial.println(ErrorMsg);
      return;
    }

    found->ch = pass_ch;
    strcpy(found->password, new_password);
  }
  else
  {
    // no more free slots for passwords
    Serial.println(ErrorMsg);
    return;
  }

  save_eeprom_data();

  Serial.println(OKMsg);
}

//----------------------------------------
// Perform a hard reboot:
//     R
//
// Reboot the device.  No need to "push the button".
//----------------------------------------

void xcmd_reboot(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "R"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // OK, reboot this thing (AtMega32U4 bootloader)
  Serial.println(F("Rebooting..."));
  reboot();
}

//----------------------------------------
// Process an external command.
//     cmd     address of command string buffer
//
// 'cmd' is '\0' terminated.
//----------------------------------------

void do_extern_cmd(char *cmd)
{
  char orig_cmd[strlen(cmd) + 1];
  strcpy(orig_cmd, cmd);

  // ensure everything is uppercase
  str2upper(cmd);

  // process the command
  switch (cmd[0])
  {
#ifdef DEBUG
    case 'X':
      xcmd_debug(cmd);
      return;
#endif
    case 'H':
      xcmd_help(cmd);
      return;
    case 'I':
      xcmd_id(cmd);
      return;
    case 'L':
      xcmd_lock(cmd);
      return;
    case 'U':
      xcmd_unlock(cmd);
      return;
    case 'C':
      xcmd_clean(cmd);
      return;
    case 'D':
      xcmd_del(cmd);
      return;
    case 'P':
      // must pass original command - don't uppercase password!
      xcmd_password(orig_cmd);
      return;
    case 'R':
      xcmd_reboot(cmd);
      return;
  }

  Serial.println(ErrorMsg);
}

//----------------------------------------
// Handle characters coming in on the Serial port.
//
// Call the command handler when a complete command is received.
//----------------------------------------

void do_external_commands(void)
{
  bool overflow = false;    // "true" if command buffer overflowed

  // gather any commands from the external controller
  while (Serial.available())
  {
    char ch = Serial.read();

    // ignore newlines
    if (ch == '\n')
      continue;

    if (CommandIndex < MAX_COMMAND_LEN)
    {
      // only add if command not over-length
      CommandBuffer[CommandIndex++] = ch;
    }
    else
      overflow = true;

    if (ch == COMMAND_END_CHAR)   // if end of command, execute it
    {
      if (overflow)
      {
        overflow = false;
        Serial.println("Command too long!");
      }
      else
      {
        CommandBuffer[CommandIndex - 1] = '\0'; // remove final ';'
        do_extern_cmd(CommandBuffer);
        CommandIndex = 0;
      }
    }
  }
}

//----------------------------------------
// check_password()
//
// Returns "true" if the proper password was entered.
// Returns "false" if no legal password yet entered, could be partial.
//----------------------------------------

bool check_password(void)
{
  int bptr = 0;   // pointer into TmpBuffer
  TmpBuffer[0] = '\0';

  while (Serial.available() > 0)
  {
    // read the incoming char
    char ch = Serial.read();

    if (ch == '\n')
    {
      // get the password string
      password *pw = find_password(LockPassword, NULL);

      if (pw == NULL)
      {
        // passord string not found!
        // this is an error, it should never happen!
        // pretend the device is unlocked
        return true;
      }

      if (strcmp(pw->password, TmpBuffer) == 0)
      {
        // match, tell caller all is OK
        return true;
      }

      // no match, try again
      bptr = 0;
      TmpBuffer[0] = '\0';
    }
    else if (ch == '\b')
    {
      if (bptr > 0)
      {
        TmpBuffer[bptr--] = '\0';
      }
    }
    else
    {
      // add new character to the saved entered string
      TmpBuffer[bptr++] = ch;
      TmpBuffer[bptr] = '\0';
    }
  }

  return false;
}

//----------------------------------------
// setup()
//
// Prepare pin mode and set current button state.
//----------------------------------------

void setup(void)
{
  // set pin modes and turn off the LED
  pinMode(ButtonPin, INPUT_PULLUP);
  pinMode(ReedPin, INPUT_PULLUP);
  pinMode(LEDPin, OUTPUT);
  digitalWrite(LEDPin, LOW);

#ifdef NUKE
  // check if button down on boot.  if so, look for "nuke" option timeout.
  if (digitalRead(ButtonPin) == LOW)
  {
    // set the timeout after which we NUKE
    unsigned long timeout = millis() + NUKE_TIMEOUT;

    while (digitalRead(ButtonPin) == LOW)
    {
      if (millis() > timeout)
      {
        // force an EEPROM initialization
        was_nuked = true;     // force restore_eeprom_data() to create false data
        Checksum += 1;
        EEPROM.put(EepromChecksum, Checksum);
        break;
      }
    }
  }
#endif

  // start Serial
  // we don't wait for serial since hangs if no Serial connected
  Serial.begin(115200);

  // prepare keyboard HID
  Keyboard.begin();

#ifdef DEBUG
  delay(2500);
#endif

  // get data from EEPROM
  restore_eeprom_data();

  // set the time for next morse "timeout"
  next_timeout = millis() + TimeoutPeriod;

  dprint("%s %s", PRODUCT_NAME, VERSION);
}

//----------------------------------------
// The loop() function.
//
// The device is either in the LOCKED mode, where it will only accept a
// password, or it's UNLOCKED and will accept any firmware command or
// button press.
//----------------------------------------

void loop(void)
{
  if (DeviceLocked)
  {
    // check if correct password is entered then unlock the device,
    // but leave locked on reboot, ie, don't update EEPROM.
    if (check_password())   // doesn't return "true" until correct password entered
    {
      DeviceLocked = false;
      Serial.println("UNLOCKED");
    }
  }
  else
  {
    // handle any commands from the serial port IF UNLOCKED
    do_external_commands();
  }

  // always check the button, do something if required
  poll_button();
}
