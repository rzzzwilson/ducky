PWKeep
======

The PWKeep.ino file is the firmware that recognizes morse input to select the
password to use.  It runs on the Ducky hardware.

The firmware is always monitoring the pushbutton for a sequence of pushes that
matches a particular saved password name.  When found, the device sends the
password through the HID Keyboard interface to the host machine.

The serial USB input is also monitored for these configuration commands:

+---------+--------------------------------------+
| Command | Meaning                              |
+=========+======================================+
| H;      | Display help text                    |
+---------+--------------------------------------+
| ID;     | Show device identifier string        |
+---------+--------------------------------------+
| CLEAN;  | Delete all saved passwords and unlock|
+---------+--------------------------------------+
| Da;     | Delete password for 'a'              |
+---------+--------------------------------------+
| P;      | Display all saved passwords          |
+---------+--------------------------------------+
| Pa;     | Display password for 'a'             |
+---------+--------------------------------------+
| Paxxx;  | Set password for 'a' to 'xxx'        |
+---------+--------------------------------------+
| LOCKa;  | Lock the device with password for 'a'|
+---------+--------------------------------------+
| UNLOCK; | Unlock the device                    |
+---------+--------------------------------------+
| R;      | Reboot the device                    |
+---------+--------------------------------------+


Python configuration
--------------------

There is a CLI python program that will allow executing the above serial
commands.


Internal details
----------------

The button code recognizes these types of logical events:

+------------------+---------+--------------------------+
| Push type        | Period  | Meaning                  |
+==================+=========+==========================+
| Short push       | *none*  | Morse "dot"              |
+------------------+---------+--------------------------+
| Long push        | >200ms  | Morse "dash"             |
+------------------+---------+--------------------------+
| Timeout          | >1000ms | Resets the state machine |
+------------------+---------+--------------------------+

