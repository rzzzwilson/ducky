Overview
========

The *PWKeep* device is a little USB stick that contains a microcontroller and a
pushbutton.  it's designed to allow you to quickly type in one of up to 16 
passwords.  You select the password to type by pressing the button to indicate
the morse character of the password you want.

*PWKeep* is not meant to be a true, secure, password manager.  If you lose the
device there is nothing to stop someone else who knows how it works from
finding out all your passwords.

Usage
-----

Suppose you have configured two passwords in your *PWKeep* device with names of
"A" and "D" respectively.  See the configuration section below to see how that
is done.

If you need to supply a password for a website, you place the cursor into the
password field on the screen and then plug in your *PWKeep* device.  Assuming
your password for this site is the "A" password, you press the button on the
device once quickly and then press again, slowly.  This inputs the morse code
for the "A" character: **· ─** .  The password associated with the "A" will
now be typed into the password field.

Configuration
-------------

"Nuke" feature
--------------

The *PWKeep* firmware has a "nuke" option.  If you plug in the device with the
button pressed down and held down for about a half a second, the device will be
cleared.  This a way to recover from a locked device that you have forgotten the
password for.  "Nuking" will give you an unlocked empty device again.
