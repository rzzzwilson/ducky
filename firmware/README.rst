Ducky firmware
==============

The Ducky hardware has various firmware loads that can be used to create
different devices.  Each directory here is a different firmware:

+----------------+----------------------------------------------+
| PWKeep         | a password keeper thing that types passwords |
+----------------+----------------------------------------------+
| mouse_jiggler  | device to "jiggle" the mouse cursor          |
+----------------+----------------------------------------------+
| haunted        | annoy a user by moving the mouse now and then|
+----------------+----------------------------------------------+

boards.txt
----------

Add the contents of *ducky_boards.txt* to the end of the
*/home/r-w/.arduino15/packages/arduino/hardware/avr/1.8.5/boards.txt* file.

Use "Ducky 3.x" in the "Tools|Board|Arduino AVR Boards" menu.

programming
-----------

Program with Arduino IDE, USBTinyISP and "Sketch|Upload using programmer".
