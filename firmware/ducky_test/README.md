The code here is used to test the *Ducky* hardware after assembly.

After burning the firmware to the device, open a Terminal window and then
plug the device in to a USB port and observe the output.  This firmware
will print the state of the "buttons" when inserted and then any button
events as they occur.  In addition, the builtin LED will blink at a 1Hz
rate.
