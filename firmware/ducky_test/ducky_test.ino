//================================================================
// Ducky test
//
// A small program to exercise the functionality of the Ducky device.
//
// Events printed are:
//    * state of the buttons on insertion
//    * button events as they occur
// The builtin LED will blink at 1 Hz.
//================================================================

#include <ezButton.h>
#include <Keyboard.h>

#define PRODUCT_NAME          "ducky_test"
#define VERSION               "0.1"

// which pin the button is attached to
#define ButtonPin             0         // Iota D2 pin

// define the debounce counter
#define DebounceCount         20        // number of millis/samples debounce delay

// create buton object on the ButtonPin pin
ezButton button(ButtonPin);

//----------------------------------------
// setup()
//
// Prepare pin mode and set current button state.
//----------------------------------------

void setup(void)
{
  Serial.begin(115200);
  delay(3000);

  // set pin mode for the button
  pinMode(ButtonPin, INPUT_PULLUP);

  // prepare keyboard HID
  Keyboard.begin();

  Serial.println("Ready");
}

//----------------------------------------
// The loop() function.
//
// The device is either in the LOCKED mode, where it will only accept a
// password, or it's UNLOCKED and will accept any firmware command or
// button press.
//----------------------------------------

int old_btnState = HIGH;

void loop(void)
{
  button.loop();

  if (button.getState() == LOW)
  {
    guess();
  }
}
