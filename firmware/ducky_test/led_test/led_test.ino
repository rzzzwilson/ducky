/*
Test the LED on a Ducky 3.x device.
*/

#define LedPin    12  // PD6
#define ButtonPin 6   // PD7
#define ReedPin   4   // PD4

void setup()
{
  Serial.begin(115200);
  delay(2500);
  pinMode(LedPin, OUTPUT);
  pinMode(ButtonPin, INPUT_PULLUP);
  pinMode(ReedPin, INPUT_PULLUP);
  Serial.println("READY");
}

void loop()
{
  int button = digitalRead(ButtonPin);
  int reed = digitalRead(ReedPin);

  Serial.print("button=");
  Serial.print(button);
  Serial.print(", reed=");
  Serial.println(reed);
  
  if (!button && !reed)
  {
    digitalWrite(LedPin, HIGH);
  }
  else
  {
    digitalWrite(LedPin, LOW);
  }

  delay(500);
}
