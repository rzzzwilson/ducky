mouse_jiggler
=============

A little bit of code using the *Ducky* hardware to turn it into a little toy
that moves the mouse in a random way, ie, a "mouse jiggler".

A short button press toggles the "jiggling" activity.  Initially the jiggling
is OFF.  When jiggling the mouse moves randomly around a saved offset point
about every half second.

If the button is held down for a long period the offset point is moved and
shown on the screen.  The next short button press saves the offset point in
EEPROM.

If the button is held down on USB insertion the device starts in "annoy"
mode.  After a long "get away" period jiggling will start automatically
and move the mouse randomly with a longer wait between moves.

The absolute move mouse code is from:

    https://github.com/jonathanedgecombe/absmouse
