import math

"""
This code generates C data to move the mouse along a programmed path.
The path is the cloverleaf symbol (⌘) drawn as four equal circles equal
distances apart connected by four crossed inner tangents.

Usage: python cloverleaf_path.py > data.c
"""

# jiggle path centred on 200,200
CX = 200
CY = 200

# radius of circles, also distance 2 circles are apart, edge to edge
radius = 10

# number of degrees in one step around a circle
d_angle = 5

# number of steps in the linear paths between circles
num_steps = 30

# counter to print "max_col" columns of path data
col_cnt = 0
max_col = 4

# start the C struct
print("{")

# top-left circle, start 0, anticlockwise, 0 -> 270 degrees
#print("top-left -------------------------------")
for angle in range(0, 270+1, d_angle):
    dx = math.cos(math.radians(angle))*radius
    dy = math.sin(math.radians(angle))*radius
    x = CX - 3*radius/2 + dx
    y = CY - 3*radius/2 - dy
#    print(f"{angle:03d}: {x:4.1f}, {y:4.1f}")
    print(f"{{{x:.0f}, {y:.0f}}}, ", end='')
    col_cnt += 1
    if col_cnt > max_col:
        col_cnt = 0
        print()

# draw straight line from bottom of top-left circle to
# bottom of top-right circle
#print("top-line -------------------------------")
x1 = CX - 3*radius/2
y1 = CY - 3*radius/2
x2 = CX + 3*radius/2
y2 = CY - 3*radius/2
x_step = (x2 - x1) / num_steps
y_step = (y2 - y1) / num_steps
for _ in range(num_steps-1):
    x += x_step
    y -= y_step
#    print(f"---: {x:4.1f}, {y:4.1f}")
    print(f"{{{x:.0f}, {y:.0f}}}, ", end='')
    col_cnt += 1
    if col_cnt > max_col:
        col_cnt = 0
        print()

# top-right circle, start 270, anticlockwise, 270 -> 180 degrees
# actually iterate 270->540, adjust to [0, 360)
#print("top-right -------------------------------")
for angle in range(270, 540+1, d_angle):
    if angle > 355:
        angle -= 360
    dx = math.cos(math.radians(angle))*radius
    dy = math.sin(math.radians(angle))*radius
    x = CX + 3*radius/2 + dx
    y = CY - 3*radius/2 - dy
#    print(f"{angle:03d}: {x:4.1f}, {y:4.1f}")
    print(f"{{{x:.0f}, {y:.0f}}}, ", end='')
    col_cnt += 1
    if col_cnt > max_col:
        col_cnt = 0
        print()

# draw straight line from left of top-right circle to
# left of bottom-right circle
#print("right-line -------------------------------")
x1 = CX + 3*radius/2
y1 = CY - 3*radius/2
x2 = CX + 3*radius/2
y2 = CY + 3*radius/2
x_step = (x2 - x1) / num_steps
y_step = (y2 - y1) / num_steps
for _ in range(num_steps-1):
    x += x_step
    y += y_step
#    print(f"---: {x:4.1f}, {y:4.1f}")
    print(f"{{{x:.0f}, {y:.0f}}}, ", end='')
    col_cnt += 1
    if col_cnt > max_col:
        col_cnt = 0
        print()

# bottom-right circle, start 180, anticlockwise, 180 -> 90 degrees
#print("bottom-right -------------------------------")
# actually iterate 180->450, adjust to [0, 360)
for angle in range(180, 450+1, d_angle):
    if angle > 355:
        angle -= 360
    dx = math.cos(math.radians(angle))*radius
    dy = math.sin(math.radians(angle))*radius
    x = CX + 3*radius/2 + dx
    y = CY + 3*radius/2 - dy
#    print(f"{angle:03d}: {x:4.1f}, {y:4.1f}")
    print(f"{{{x:.0f}, {y:.0f}}}, ", end='')
    col_cnt += 1
    if col_cnt > max_col:
        col_cnt = 0
        print()

# draw straight line from top of bottom-right circle to
# top of bottom-left circle
#print("bottom-line -------------------------------")
x1 = CX + 3*radius/2
y1 = CY + 3*radius/2
x2 = CX - 3*radius/2
y2 = CY + 3*radius/2
x_step = (x2 - x1) / num_steps
y_step = (y2 - y1) / num_steps
for _ in range(num_steps-1):
    x += x_step
    y -= y_step
#    print(f"---: {x:4.1f}, {y:4.1f}")
    print(f"{{{x:.0f}, {y:.0f}}}, ", end='')
    col_cnt += 1
    if col_cnt > max_col:
        col_cnt = 0
        print()

# bottom-left circle, start 90, anticlockwise, 90 -> 360 degrees
#print("bottom-left -------------------------------")
for angle in range(90, 360+1, d_angle):
    dx = math.cos(math.radians(angle))*radius
    dy = math.sin(math.radians(angle))*radius
    x = CX - 3*radius/2 + dx
    y = CY + 3*radius/2 - dy
#    print(f"{angle:03d}: {x:4.1f}, {y:4.1f}")
    print(f"{{{x:.0f}, {y:.0f}}}, ", end='')
    col_cnt += 1
    if col_cnt > max_col:
        col_cnt = 0
        print()

# draw straight line from right of bottom-left circle to
# right of top-left circle
#print("left-line -------------------------------")
x1 = CX - 3*radius/2
y1 = CY + 3*radius/2
x2 = CX - 3*radius/2
y2 = CY - 3*radius/2
x_step = (x2 - x1) / num_steps
y_step = (y2 - y1) / num_steps
for _ in range(num_steps-1):
    x += x_step
    y += y_step
#    print(f"---: {x:4.1f}, {y:4.1f}")
    print(f"{{{x:.0f}, {y:.0f}}}, ", end='')
    col_cnt += 1
    if col_cnt > max_col:
        col_cnt = 0
        print()

print()

# terminate the struct
print("};")
