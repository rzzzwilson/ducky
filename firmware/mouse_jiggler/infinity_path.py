import math

"""
This code generates C data to move the mouse along a programmed path.
The path is the infinity symbol (∞) drawn as two equal circles one radius
apart connected by two crossed tangents.

Usage: python infinity_path.py > data.c
"""

# jiggle path centred on 200,200
CX = 200
CY = 200

# radius of circles, also distance 2 circles are apart, edge to edge
radius = 10

# number of degrees in one step around a circle
d_angle = 5

# number of steps in the linear paths between circles
num_steps = 20

# counter to print "max_col" columns of path data
col_cnt = 0
max_col = 4

# start the C struct
print("{")

# left circle, start 45, anticlockwise, 45 -> 315 degrees
for angle in range(45, 315+1, d_angle):
    dx = math.cos(math.radians(angle))*radius
    dy = math.sin(math.radians(angle))*radius
    x = CX - 3*radius/2 + dx
    y = CY - dy
#    print(f"{angle:03d}: {x:4.1f}, {y:4.1f}")
    print(f"{{{x:.0f}, {y:.0f}}}, ", end='')
    col_cnt += 1
    if col_cnt > max_col:
        col_cnt = 0
        print()

# draw straight line from (192.1, 207.1) to (207.9, 192.9), "num_steps" steps
x_step = (207.9 - 192.1) / num_steps
y_step = (207.1 - 192.9) / num_steps
for _ in range(num_steps - 1):
    x += x_step
    y -= y_step
#    print(f"---: {x:4.1f}, {y:4.1f}")
    print(f"{{{x:.0f}, {y:.0f}}}, ", end='')
    col_cnt += 1
    if col_cnt > max_col:
        col_cnt = 0
        print()

# right circle, start 135, clockwise, 135 -> -135
for angle in range(135, -135-1, -d_angle):
    dx = math.cos(math.radians(angle))*radius
    dy = math.sin(math.radians(angle))*radius
    x = CX + 3*radius/2 + dx
    y = CY - dy
#    print(f"{angle:03d}: {x:4.1f}, {y:4.1f}")
    print(f"{{{x:.0f}, {y:.0f}}}, ", end='')
    col_cnt += 1
    if col_cnt > max_col:
        col_cnt = 0
        print()

# draw straight line from (207.9, 207.1) to (192.1, 192.9)
x_step = (207.9 - 192.1) / num_steps
y_step = (207.1 - 192.9) / num_steps
for _ in range(num_steps - 1):
    x -= x_step
    y -= y_step
#    print(f"---: {x:4.1f}, {y:4.1f}")
    print(f"{{{x:.0f}, {y:.0f}}}, ", end='')
    col_cnt += 1
    if col_cnt > max_col:
        col_cnt = 0
        print()
print()

# terminate the struct
print("};")
