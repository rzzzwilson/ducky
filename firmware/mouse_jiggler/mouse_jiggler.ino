//********************************************************
// Simple bit of code to move the mouse a small random amount
// every now and then when jiggling is ON.  Short presses
// of the button toggle jiggling between ON and OFF.
//
// A long press of the button moves the jiggle point.
//
// If the button is held down during insert the device goes
// into "annoy" mode, with infrequent random moves.
//
// This firmware is for the "ducky" hardware device.
//********************************************************

#include <math.h>
#include <EEPROM.h>
#include "AbsMouse.h"
#include <ezButton.h>

#define version	"2.2"

//********************************************************
// Pin connections.
//********************************************************

// pin the button is attached to
const int ButtonPin = 0;        // atmega32u4 pin 20, PD2, 0 on leonardo

//********************************************************
// Jiggler state variables
//********************************************************

// if and when to jiggle
bool jiggling = false;                // "true" if jiggling
unsigned long next_jiggle = 0;        // scheduled time for next jiggle

// delay between movements (milliseconds)
const int JiggleDelay = 500;

// if the button is down when the ducky is inserted, go into annoying mode
// the min/max times are the range of random pauses between updates in seconds
// the "escape_period" is the time to wait before annoying
// the (uint64_t) cast is to force 64 bits of calculation
bool annoy_mode = false;                              // if true, random slow moves
unsigned long min_annoy_time = ((uint64_t) 10*1000);  // 10 seconds in msec
unsigned long max_annoy_time = ((uint64_t) 20*1000);  // 20 seconds in msec
unsigned long escape_period = ((uint64_t) 300*1000);  // escape delay (5 mins) in msec
                                                      // (time to get away!)
                                                      
// offset in X and Y from top-left
// sets point (JiggleOffset,JiggleOffset) around which we jiggle
const int MinJiggleOffset = 200;
const int MaxJiggleOffset = 800;

// how much we move jiggle origin on LONG event
const int JiggleDelta = 20;

//********************************************************
// The ezbutton object and state stuff
//********************************************************

ezButton button(ButtonPin);

// button + events state variables
unsigned long NextPushCheck = 0;      // time for next check
const unsigned long PushDelay = 300;  // time before short recognized
const unsigned long LongDelay = 300;  // extra time before long recognized

const unsigned int BUTTON_STATE_NONE = 0;   // states returned from button code
const unsigned int BUTTON_STATE_SHORT = 1;
const unsigned int BUTTON_STATE_LONG = 2;

int button_state = BUTTON_STATE_NONE; // initial button state

//********************************************************
// EEPROM data stuff.
//********************************************************

// default settings, these are put into EEPROM if checksum doesn't match
const unsigned int DefaultJiggleOffset = MinJiggleOffset;

// in-memory copy of EEPROM data
unsigned int JiggleOffset = MinJiggleOffset;

// the EEPROM data CRC
unsigned long Checksum = 0;

// define start addresses in EEPROM of various fields
const int EepromAddrChecksum = 0;
const int EepromAddrData = EepromAddrChecksum + sizeof(Checksum);

//********************************************************
// Code to handle data in the EEPROM.
//--------------------------------------------------------
// Calculate the CRC of the data in memory
// This code from: https://www.arduino.cc/en/Tutorial/LibraryExamples/EEPROMCrc
//--------------------------------------------------------

unsigned long eeprom_crc(void)
{
  const unsigned long crc_table[16] =
  { 0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
    0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
    0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
    0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
  };
  unsigned long crc = ~0L;

  for (unsigned long index = EepromAddrData; index < sizeof(JiggleOffset); ++index)
  {
    byte eeprom_byte = EEPROM.read(index);
    
    crc = crc_table[(crc ^ eeprom_byte) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (eeprom_byte >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }

  return crc;
}

//--------------------------------------------------------
// Saves the in-memory data to EEPROM and refreshes the CRC.
//--------------------------------------------------------

void save_eeprom_data(void)
{
  EEPROM.put(EepromAddrData, JiggleOffset);

  Checksum = eeprom_crc();
  EEPROM.put(EepromAddrChecksum, Checksum);
}

//--------------------------------------------------------
// Gets the data in EEPROM and refreshes the RAM copies.
//
// If the EEPROM checksum is invalid, sets EEPROM and
// in-memory copy to default values.
//--------------------------------------------------------

void restore_eeprom_data(void)
{
  // get expected data checksum
  EEPROM.get(EepromAddrChecksum, Checksum);

  // if checksum wrong, initialize EEPROM
  if (Checksum != eeprom_crc())
  {
    // force values to the default state
    JiggleOffset = DefaultJiggleOffset;

    // and save initialized data (also updates EEPROM checksum)
    save_eeprom_data();
  }

  // get data stored in EEPROM
  EEPROM.get(EepromAddrData, JiggleOffset);
}

//********************************************************
// Check the button and return an event: NONE, SHORT or LONG press.
//--------------------------------------------------------

int check_press(unsigned long now)
{
  // read button state
  button.loop();

  // get current button state
  bool button_down = !button.getState();

  // convert to BUTTON_STATE_SHORT/BUTTON_STATE_LONG/BUTTON_STATE_NONE event
  switch (button_state)
  {
    case BUTTON_STATE_NONE:
      if (button_down)
      {
        button_state = BUTTON_STATE_SHORT;
        NextPushCheck = now + PushDelay;
      }
      break;
    case BUTTON_STATE_SHORT:
      if (!button_down)
      {
        button_state = BUTTON_STATE_NONE;
        return BUTTON_STATE_SHORT;
      }

      // button still down, long enough?
      if (now >= NextPushCheck)
      {
        NextPushCheck = now + LongDelay;
        button_state = BUTTON_STATE_LONG;
        return BUTTON_STATE_LONG;
      }
      break;
    case BUTTON_STATE_LONG:
      if (!button_down)
      {
        button_state = BUTTON_STATE_NONE;
        break;
      }

      if (now >= NextPushCheck)
      {
        NextPushCheck = now + PushDelay;
        return BUTTON_STATE_LONG;
      }
      break;
  }
  
  return BUTTON_STATE_NONE;
}

//********************************************************
// Prepare the device.
//********************************************************

void setup()
{
  unsigned long now = millis();
  
  // define pin usage, check if button down on boot
  // if pressed, go into "annoy" mode
  pinMode(ButtonPin, INPUT_PULLUP);
  
  if (digitalRead(ButtonPin) == LOW)
  {
    // button is down, use "annoying" mode
    annoy_mode = true;
    
    next_jiggle = now + escape_period;  // delay first jiggle
    jiggling = true;

    // wait until button is released
    while (digitalRead(ButtonPin) == LOW)
    {
      delay(10);
    }
  }

  // seed the Random Number Generator
  randomSeed(analogRead(A0));

  // start using button as ezButton, set debounce
  button.setDebounceTime(50);
  for (int i=0; i < 5; ++i)
  {
    delay(100);
    button.loop();
  }

  // start the mouse
  AbsMouse.init(1000, 1000);

  restore_eeprom_data();
}


//********************************************************
// Just check the button for a press.
// Short press turn jiggle ON or OFF.
// Long press move jiggle origin.
//********************************************************

void loop()
{
  // get current time
  unsigned long now = millis();

  // check for button event
  int event = check_press(now);

  // LONG press adjust the jiggle origin
  if (event == BUTTON_STATE_LONG)
  {
    // if jiggling, disable that
    jiggling = false;
    
    // move jiggle origin
    JiggleOffset += JiggleDelta;
    if (JiggleOffset > MaxJiggleOffset)
    {
      JiggleOffset = MinJiggleOffset;
    }

    // show where new origin is
    AbsMouse.move(JiggleOffset, JiggleOffset);
    
    return;
  }

  // if SHORT turn jiggle mode on/off
  if (event == BUTTON_STATE_SHORT)
  {
    // annoy mode off if it was set
    annoy_mode = false;
    
    // toggle the jiggle state
    jiggling = !jiggling;

    if (jiggling)
    {
      // schedule next jiggle
      next_jiggle = now + random(JiggleDelay, 2*JiggleDelay);

      // save any new offset when jiggling starts
      save_eeprom_data();
    }

    return;
  }

  // if we are jiggling and it's time for the next jiggle, do it!
  // don't need to worry about millis() rollover (49+ days)
  if (jiggling && (next_jiggle <= now))
  {
    // schedule next jiggle
    if (annoy_mode)
    {
      next_jiggle += random(min_annoy_time, max_annoy_time);
    }
    else
    {
      next_jiggle += random(JiggleDelay, 2*JiggleDelay);
    }
    
    // move to next random point around (JiggleOffset, JiggleOffset)
    int new_x = JiggleOffset + random(40);
    int new_y = JiggleOffset + random(40);

    AbsMouse.move(new_x, new_y);
  }
}
