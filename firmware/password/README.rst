password
========

This is a very simple version of the PWKeep software.  It just sends a password
to the PC over the USB port.  The password is compiled into the code.  All the
extra features in PWKeep are deleted in this code.
