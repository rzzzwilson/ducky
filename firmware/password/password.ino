//================================================================
// password
//
// A small program which stores one password and sends it to the
// host as keyboard input when the button is pressed.
//
// This code runs on an AtMega32U4 microcontroller with a USB
// port and a button on pin 0.
//================================================================

#include <Keyboard.h>
#include <ezButton.h>

#define PRODUCT_NAME          "password"
#define VERSION               "0.1"

// which pin the button is attached to
#define ButtonPin             0

// define the debounce counter
#define DebounceCount         20        // number of millis/samples debounce delay

// the password
const char *Password = "correct horse battery staple";

// the button object
ezButton button(ButtonPin);


//----------------------------------------
// setup()
//----------------------------------------

void setup(void)
{
  button.setDebounceTime(DebounceCount);
  
  // prepare keyboard HID
  Keyboard.begin();
}

//----------------------------------------
// The loop() function.
//
// Check the button.  If pressed, emit password.
//----------------------------------------

void loop(void)
{
  button.loop();
  
  if (button.isPressed())
  {
    Keyboard.print(Password);
  }
}
