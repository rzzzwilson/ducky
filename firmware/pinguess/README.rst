pinguess
========

A program that runs on the Ducky hardware to guess a phone user's PIN.

Plug the Ducky hardware into the phone/tablet and press the button!

Inspired by:

https://hackaday.com/2023/07/16/brute-forcing-a-mobiles-pin-over-usb-with-a-3-board/

Status
------

Works, but with the enhanced security in Android, not very useful.

Maybe if using only the 10 most common 4 digit PINs as mentioned in the article.