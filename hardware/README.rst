Ducky
=====

A simple little "AtMega32U4 on a USB stick" thing that can be used for
various things that behave like a mouse or keyboard.

Directories
-----------

+---------------+--------------------------------------+
| Directory     | Usage                                |
+===============+======================================+
| kicad         | The kicad project for the Ducky PCB  |
+---------------+--------------------------------------+

Schematic
---------

The schematic shows the device to be very simple, hardware-wise.
Just an AtMega32U4 with support components and a pushbutton:

.. image:: Ducky_2.0_Schematic.png

Most of the complexity is in the firmware.

PCB
---

Initially the design used an AtMega32U4 in the QFN44 package, but this was
a problem as soldering the tiny package wasn't easy, though possible.

.. image:: Ducky.JPG

In the interests of ease of construction the QFN44 package was changed to the
QFP44 version:

.. image:: Ducky_2.0.png

Bootloader Installation
-----------------------

The bootloader is installed using the USBTinyISP programmer.  Attach the
programmer to a 6 pin 2.0 mm linear set of pogo pins.  The programming lines
must connect to the correct position in the ICSP holes on the board.

Start the arduino IDE and choose the Leonardo board.  Set the programmer
to "USBTinyISP".  Connect the programmer to the USB port and ensure the IDE
connects to it.  Then place the 6 pin pogo pins into the ICSP pins on
the Ducky board, ensuring that the #1 pogo pin goes into the hole marked "1".
The select "burn bootloader" from the menu.  Be patient, burning the bootloader
takes a while.

Programming
-----------

Find the Arduino *boards.txt* file and put the data in the *ducky_boards.txt*
file into it at the end.  Start the IDE and you will see the "Ducky" entry
at the end of the AVR boards.  You don't need to do this to program the
board, but you do need to use the Ducky board if you want to auto-recognize
it from the python code (*pwkeep*).  Program as for the Leonardo board.

History
=======

The hardware was initially designed to be used for the "password keep"
device so was called *PWKeep*.  But the hardware was found to be generally
useful with other firmware so the hardware was renamed to *Ducky*, in a nod
to the
`Rubber Ducky <https://hak5.org/products/usb-rubber-ducky-deluxe>`_
version of the
`BadUSB <https://en.wikipedia.org/wiki/BadUSB>`_
USB-based malware device.

My *Ducky* has nothing in common with the *Rubber Ducky* commercial product.
